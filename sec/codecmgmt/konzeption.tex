\subsection{Konzeption}
\label{sec:codec:konzept}
\subsubsection{Übersicht}
Das Konzept beruht auf dem von Ansatz des \ac{SigOfly}-Mechanismus des 
\ac{WONDER}v1 Frameworks -- dem Nachladen von Funktionalitäten und Protokollen 
zur Laufzeit in das Framework.

Ein Datenkanal kann entweder verschiedene Formate übertragen oder nur eines.
Bei letzterem Ansatz müsste pro neuem Format ein eigener Kanal etabliert werden.
In jedem Fall muss jedoch pro \ac{P2P}-Verbindung mindestens 
ein Kanal aufgebaut werden,
wenn ein Datenkanal genutzt werden soll,
um dem \ac{WebRTC}-Prinzip gerecht zu werden.
Beide Ansätze haben verschiedene Vor- und Nachteile, welche in  
\autoref{tab:channelformat} gegeneinander abgewogen werden.

\begin{savenotes}
\begin{table}[H]
\centering
\begin{tabular}{p{4.3cm}|p{4.3cm}|p{6cm}}
 Parameter & 1 Kanal pro Format & 1 Kanal mit mehreren Formaten \\
 \hline
 Overhead\footnote{Overhead bezeichnet die Gesamtmenge der Daten abzüglich der 
 Nutzdaten.} & Beim Aufbau des Kanals & Beim 
 Wechsel des Payload"-typs \\
 Notwendigkeit eines \newline Aushandlungsprotokolls & Nein, 1 Kanal pro 
 Payload"-typ & Ja, 
 Aushandlung im Kanal \\
 Zentrale Payloadtypen- \newline verwaltung & Nein & Nein \\
 Zentrale Verwaltung \newline von Codecs & Nein & Nein \\
 Zusätzliche Kapselung \newline von Daten & Nein & Ja \\
\end{tabular}
\caption{Gegenüberstellung von Formaten in Datenkanälen}
\label{tab:channelformat}
\end{table}
\end{savenotes}

Des Weiteren haben sich die Autoren Gedanken über die Inhalte des Datenkanals gemacht.
So wurde zwischen Payloadtypen, Formaten und Codecs unterschieden.

Payloadtypen wurden als Anwendungsfallidentifikator deklariert.
So ist ein Anwendungsfall beispielhaft eine Chat-Kommunikation und damit ein Payloadtyp.
Formate beschreiben die Kapselungsart oder die Darstellung von Daten.
Ein Format kann beispielsweise Base64 sein, welches Daten Base64-codiert darstellt,
jedoch auch \ac{XML}, welches Daten in Tags kapselt, um sie zu beschreiben 
\cite{rfc4648,w3c:xml}.
Codecs sind Algorithmen zur Umwandlung von Daten.
Die Autoren haben ihnen weiterhin einen Objektcharakter zugewiesen,
sodass diese Referenzen, Funktionen und Daten halten können.

\subsubsection{Payloadtypen teilen sich einen Kanal}
Wenn ein Kanal für jede Art von Daten genutzt werden soll,
muss eine Unterscheidung der verwendeten Typen im Kanal stattfinden.
Da ein entfernter Teilnehmer nicht wissen kann, wann welche Typen von Daten 
beginnen oder enden,
muss ein Verfahren genutzt werden, das dieses Problem behebt.

Nachfolgend sind Lösungsansätze aufgezählt:

\begin{enumerate}
\item Der Anfang und das Ende von Daten sind im \ac{TLV} Stil angegeben.
\item Daten werden in ein festgelegtes Format gekapselt (z.B. \ac{JSON} oder \ac{XML}).
\item Ein reservierter String gibt an, welches Format als nächstes benutzt wird.
\end{enumerate}

Bei der ersten Lösung müssen beide Teilnehmer ein fest definiertes Protokoll 
nutzen,
welches angibt, wie das \ac{TLV}-Feld gegliedert ist und wie die Abgrenzungen 
von den \ac{TLV}-Komponenten definiert sind.
Der Nachteil hierbei ist die maximale Größe der zu übertragenden Daten,
welche durch die Angabe eines Wertes im Längen-Feld begrenzt wäre.
Zusätzlich muss vor jeder Übertragung die Größe der Daten bekannt sein,
was nicht in jedem Fall gewünscht oder möglich ist. 
Ein Beispiel hierfür ist das Auslesen von Daten aus einem 
Data-Mining-Programm\footnote{Data-Mining bezeichnet die methodische und 
algorithmische Anwendung von Funktionen zum Extrahieren empirischer 
Zusammenhänge von Objekten einer Datenbasis.}.

Der zweite Ansatz hat den Nachteil, dass ein festgelegtes Format verwendet 
werden muss
und dieses nicht oder nur über Umwege verschiedene Datentypen fassen kann.
So können über \ac{XML} nur im \ac{CDATA}-Feld verschiedene Daten gefasst 
werden,
welche dennoch \textit{escaped}\footnote{Als Escapen 
oder 
auch 
Escape-Sequenz bezeichnet 
man einen Vorgang, bei dem durch eine spezielle Kombination von Zeichen die 
Folgesymbolfolge als Sonderzeichen interpretiert wird und demzufolge anders als 
ohne Escape-Sequenz
ausgewertet werden kann.} werden müssen, um den Abschlussidentifizierer nicht zu 
beinhalten.
Im \ac{JSON}-Format müssten die Daten ebenfalls escaped oder in Base64 
umgewandelt werden.

Ansatz drei hat den Nachteil, dass über alle Daten ein 
Pattern-Matching\footnote{Pattern-Matching bezeichnet einen Abgleich von 
Mustern bzw. eine musterbasierte Suche und beschreibt das Verfahren, mit dem 
anhand eines vorgegebenen Musters in einer Symbolfolge nach einer Menge oder 
Teilmenge dieses Musters gesucht wird.} 
erfolgen muss,
um den reservierten String zu finden, welcher den nächsten Datentyp angibt.
Da ein Pattern-Matching für $n$ Byte $n$ Operationen benötigt,
würde das Framework bei großen Datenmengen schlechter skalieren als
die anderen Varianten.

\subsubsection{Payloadtypen haben eigene Kanäle}

Wenn für jeden Payloadtyp ein eigener Kanal existiert,
muss die Aushandlung des zu verwendenden Typs bei seiner Initialisierung innerhalb des Kanals
oder außerhalb des Kanals erfolgen.

Um den Typ innerhalb des Kanals zu bestimmen, wird nach dem Aufbau ein 
Payloadtyp übertragen.
Dieser reicht jedoch nicht aus, um die zu übertragenen Daten hinreichend zu beschreiben,
da er lediglich einen Anwendungsfall beschreibt, nicht jedoch die Art der Codierung oder des Formats.
Um dem entgegenzuwirken, könnte nach dem Payloadtyp ebenfalls das Format und 
der Codec angegeben werden.


Wenn der Payloadtyp außerhalb des Kanals ausgehandelt werden soll,
müssen Nachrichten definiert werden,
welche zwischen dem Framework und den Messaging Servern übertragen werden.
Diese Nachrichten müssen ebenfalls den Typ,
das Format und den zu verwendenden Codec beinhalten.

Die Aushandlung außerhalb des Kanals hat den Vorteil,
dass der Frameworknutzer die Möglichkeit hat,
den Aufbau des Datenkanals oder des Payloadtyps abzulehnen,
während bei der Aushandlung im Kanal dieser zuvor aufgebaut werden muss.


\subsubsection{Codecs}

Codecs sollen den Datenkanal und den zugehörigen Eventhandler referenzieren,
um die Schnittstelle zum entfernten Teilnehmer und zum Framework zu bedienen.
Ebenso soll ein Codec Funktionen zur Codierung und Decodierung der zu 
empfangenden
und zu übertragenden Daten beinhalten.
Ein Codec stellt den zentralen Punkt dar, welcher den Datenkanal, den 
Payloadtypen des 
Datenkanals und den zu verwendenden Codierungs- und Decodierungsalgorithmus
und damit auch das entsprechende Format bildet.

Um dem Anspruch gerecht zu werden, beliebige Codecs für Datenkanäle verwenden zu können,
dürfen keine statischen Codecs definiert werden,
welche bei jedem Update des \ac{WONDER}v2-Frameworks mitgeliefert werden 
müssten.
Codecs sollen vom Nutzer des Frameworks zur Laufzeit 
nachgeladen werden können.
Der Entwickler hat somit die Möglichkeit, beliebige Codecs nachzuladen und 
beliebige Funktionen in diese zu implementieren.

Die Autoren entschieden sich dafür, den \ac{URI},
mit dem die Codecs nachgeladen werden,
als Schlüssel für den jeweiligen Codec zu nutzen.
Da jeder Codec eine eindeutige Herkunft hat, ist dieses Modell gewählt worden.
Des Weiteren hat der Ansatz den Vorteil,
dass ein Codec für eine bestimmte Domäne nur innerhalb dieser erneuert werden 
muss,
um die Änderung zu jedem Endpunkt übertragen zu können.

Der Schlüssel wird als Referenz für jeden Datenkanal des lokalen Nutzers zum entfernten Teilnehmer und umgekehrt genutzt.
Da Datenkanäle bidirektional Daten übertragen können,
werden bei einer bidirektionalen Kommunikation zwei Schlüssel pro Datenkanal 
benötigt.

Codecs, welche bereits einen zentralen Punkt für alle datenkanalabhängigen 
Funktionalitäten bilden, müssen ebenfalls identifiziert werden und logisch 
auffindbar sein.
Die Autoren haben sich dazu entschlossen, dafür ein Objekt zu verwenden,
welches Eigenschaften der Codecs nutzt, um eine logische Hierarchie abbilden zu können.
Ein Objekt ist an dieser Stelle sinnvoll, da es \ac{KVP}s nutzen kann,
um mittels eines eindeutig festgelegten Schlüssels auf Unterobjekte zugreifen zu können.
Hingegen kann auf die Elemente eines Arrays nur mittels des Indizes und nicht mit einem
selbst definierten Schlüssel zugegriffen werden.

Die Autoren wollten Kollisionsresistenz gewährleisten,
weshalb die in \autoref{fig:codec:maptree} dargestellte baumähnliche Struktur 
entworfen 
wurde, 
welche in einer hier vertikalen Ebene mit gleichem Elternknoten eindeutige 
Schlüssel benutzt.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{gv/codecmap.eps}
	\caption[Struktur der Codecmap]{Struktur der Codecmap aus Sicht von Alice}
	\label{fig:codec:maptree}
\end{figure}

Das Element \textit{Codecmap} stellt die Wurzel des Baums dar, welche das 
Objekt ist,
das die Datenstruktur enthält und mit welchem auf diese zugegriffen werden kann.
Das Element \textit{from} repräsentiert die Ebene des Baumes, welche den 
Schlüssel zu einem Teilnehmer enthält,
von welchem aus der Datenstrom gesendet wird, für den der entsprechende Codec in der letzten Ebene genutzt wird.
Das \textit{to}-Element beinhaltet den Schlüssel zu jenem Teilnehmer,
zu welchem der Datenstrom vom Teilnehmer eine Ebene darüber gesendet wird und dessen Codec der in der letzten Ebene ist.
Das letzte Element repräsentiert den Payloadtypen und beinhaltet zum einen den 
\ac{URI} zum zugehörigen Codec und zum anderen eine Instanz des Codecs,
welcher für diesen unidirektionalen Kommunikationsfall benutzt wird. Weiterhin 
enthält es den zugehörigen \texttt{DataChannelEvtHandler}.
