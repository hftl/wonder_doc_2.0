\subsection{\acs{WONDER}v1}

WebRTC stellt eine Spezifikation für den einheitlichen Austausch von Nutzdaten 
zwischen mehreren Browsern dar, spezifiziert jedoch nicht, auf welche Art und 
Weise die Signalisierung zwischen den Browsern stattfindet. 
Die WebRTC-Architektur basiert auf zwei unterschiedlichen Modellen. Zum einen 
kann die Dreiecksstruktur genutzt werden, bei der ein zentraler Webserver zum 
Austausch der Signalisierungsnachrichten zwischen Browser A und B genutzt wird.
Die zweite Möglichkeit bildet die trapezförmige Struktur, bei der die Browser A
und B auf unterschiedliche Webserver zugreifen. In diesem Fall muss die 
Signalisierung 
zwischen den beiden Webservern stattfinden. \autoref{fig:webrtc:architecture} 
zeigt eine schematische Darstellung dieser beiden Architekturformen.


% TODO: Signalling -> Signaling in dem Bild, damit es einheitlich ist
\begin{figure}[H]
	\centering
	\begin{subfigure}[b]{0.49\textwidth}
		\includegraphics[width=\textwidth]{pic/webrtc-triangle.eps}
		\caption{WebRTC Triangle (vgl. \cite{webrtcbook})}
		\label{fig:webrtc:triangle}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\textwidth}
		\includegraphics[width=\textwidth]{pic/webrtc-trapezoid.eps}
		\caption{WebRTC Trapezoid (vgl. \cite{webrtcbook})}
		\label{fig:webrtc:trapezoid}
	\end{subfigure}
	\caption{Architekturformen der WebRTC-Signalisierung}
	\label{fig:webrtc:architecture}
\end{figure}

Die Aushandlung der Medieneigenschaften erfolgt in jedem Fall über das 
\ac{SDP} \cite{rfc4566,draft-nandakumar-rtcweb-sdp-08}.
An dieser Stelle 
haben Entwickler von WebRTC-Anwendungen einen hohen Freiheitsgrad. Während 
viele Anbieter auf standardisierte Signalisierungslösungen 
wie das \ac{SIP} over Websocket setzen, können auch völlig 
eigene 
Signalisierungsprotokolle 
implementiert werden \cite{rfc7118,rfc3261}.
 
Dies bietet Betreibern von WebRTC-Plattformen einerseits ein hohes Maß an 
Flexibilität, 
sorgt jedoch auf der anderen Seite für das Entstehen von in sich geschlossenen 
Insellösungen.

Um dieser Problematik entgegenzuwirken, beschäftigten sich die
\ac{T-Labs}
und die \ac{PT} Inovação e 
Sistemas 
im Rahmen eines 
durch die Europäische 
Kommission finanzierten Projektes mit den folgenden 
Fragestellungen\cite{webrtchacks:wonder}:
\begin{itemize}
	\item Was passiert, wenn der Anrufer teilweise die Kontrolle über den Anruf
	behalten möchte?
	\item Wer bestimmt die Kommunikationsplattform?
	\item Wie erlaubt man domänenübergreifende Kommunikationen?
	\item Wie vermeidet man, dass ein Anbieter seine Nutzer zwingt, proprietäre
	Protokolle zu verwenden?
\end{itemize}

Resultat dessen war das \ac{WONDER}-Projekt mit dem \ac{SigOfly}-Mechanismus, 
welcher es 
ermöglicht, mit 
Teilnehmern fremder Domains zu kommunizieren, ohne  zuvor Kenntnisse über die 
spezifische Signalisierung in diesen Domains zu 
haben\cite{ieee:sigofly,gh:wonder}. Dabei 
liefert 
ein Server 
JavaScript-Code-Fragmente aus (\textit{Stubs}), welche Informationen über die 
Signalisierung in 
der 
fremden Domain enthalten.
Nachfolgend wird dieser Mechanismus näher untersucht und es wird auf die 
Vor- und 
Nachteile des Konzeptes eingegangen.

Der \ac{SigOfly}-Mechanismus besteht aus vier Komponenten:
\begin{itemize}
	\item \textbf{Domain Channel:} Ist der Signalisierungskanal, welcher 
	aufgebaut 
	wird, sobald sich der Nutzer an seiner Domain anmeldet.
	\item \textbf{Transient Channel:} Ist der Signalisierungskanal, der im 
	Rahmen 
	einer Konversation aufgebaut wird. Dieser Kanal kann sowohl zur eigenen 
	Domain (inter-call) oder zu einer fremden Domain (intra-call) aufgebaut 
	werden.
	\item \textbf{Messaging Stub:} Ist ein JavaScript-Code-Fragment, welches 
	den 
	Protokollstapel für die Signalisierung sowie Funktionen zum Aufbau 
	einer Verbindung mit 
	einem Messaging-Server enthält.
	\item \textbf{Conversation Host:} Ist der Messaging Server, der zur 
	Verwendung des Nachrichtenaustausches zwischen den 
	Kommunikationsteilnehmern der verschiedenen Domains genutzt wird.
\end{itemize}

Beim Aufbau einer Verbindung von Alice zu Bob gibt es zwei Varianten, wie die 
Kommunikation erfolgen kann:
\subsubsection*{Variante 1: Der Angerufene verwaltet die Konversation}
Verwaltet der Angerufene die Konversation, erfolgt die Signalisierung über die 
Domain des 
Angerufenen (Bob) und dessen Messaging Server. Dabei sind die folgenden 
Schritte notwendig, welche sich auf 
\autoref{fig:sigofly1} beziehen:
\begin{enumerate}
	\item Informationen zur Identität von Bob, seinem Messaging Stub und seinem
	Messaging Server werden durch den \ac{IdP} von Bob bereitgestellt. 
	\item Alice lädt sich den Messaging Stub von Bob herunter.
	\item Der Transient Channel zu Bobs Domain wird über den Messaging Stub 
	aufgebaut. Anschließend wird eine Invitation-Nachricht mit der 
	\ac{SDP}-Offer\footnote{Die \ac{SDP}-Offer gibt die Sitzungsbeschreibung 
	aus Sicht des Anrufers an.} an Bob gesendet.
	\item Bob ist mit dem selben Messaging Server wie Alice über den Domain 
	Channel verbunden und empfängt die Invitation-Nachricht von Alice. 
	Anschließend wird die \ac{SDP}-Answer\footnote{Die \ac{SDP}-Answer gibt die 
	Sitzungsbeschreibung 
		aus Sicht des Angerufenen an.} erstellt und zu Alice geschickt.
	\item Mit Erhalt der \ac{SDP}-Answer wird die \ac{P2P}\footnote{
	Eine \ac{P2P}-Verbindung ist eine Verbindung zwischen
	zwei gleichberechtigten Punkten in einer Kommunikationsbeziehung.}
	Medienverbindung zwischen Alice und Bob aufgebaut.
\end{enumerate}
\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{pic/sigofly1.eps}
	\caption[\acs{SigOfly}: Konversation bereitgestellt durch 
	Bob]{Konversation, 
	bereitgestellt vom Angerufenen (Bob) (vgl. 
	\cite{webrtchacks:wonder})}
	\label{fig:sigofly1}
\end{figure}


\subsubsection*{Variante 2: Der Anrufende verwaltet die Konversation}
Bei Nutzung dieser Variante wird der Messaging Server des Anrufenden genutzt 
und es sind folgende Abläufe notwendig (\autoref{fig:sigofly2}):
\begin{enumerate}
	\item Ein Benachrichtigungsendpunkt sendet eine Benachrichtigung an Bob 
	mit der \texttt{In"-vitation}-Nachricht von Alice, welche die 
	\ac{SDP}-Offer 
	enthält.
	\item Die Identität von Alice inklusive des Endpunktes mit dem
	Messaging-Stub-\ac{URI} werden von Alice' \ac{IdP} bereitgestellt.
	\item Nach dem Akzeptieren der \texttt{In"-vitation}-Nachricht lädt Bob 
	den 
	Messaging 
	Stub von Alice herunter, instanziiert diesen und baut somit den Transient 
	Channel auf.
	\item Bob sendet seine \ac{SDP}-Answer an Alice.
	\item Die \ac{P2P}-Medienverbindung zwischen Alice und Bob wird 
	hergestellt und der Nutzdatenaustausch kann erfolgen.
\end{enumerate}
\begin{figure}[H] % TODO Nachricht 4 geht an einen Messaging Server, nicht 
%direkt zu Alice
	\centering
	\includegraphics[width=\textwidth]{pic/sigofly2.eps}
	\caption[\acs{SigOfly}: Konversation bereitgestellt durch 
	Alice]{Konversation, bereitgestellt vom Anrufenden (Alice) (vgl. 
	\cite{webrtchacks:wonder})}
	\label{fig:sigofly2}
\end{figure}

Die \ac{WONDER}v1-JavaScript-Bibliothek stellt eine Referenzimplementierung des 
\ac{SigOfly}-Mechanismus dar und wurde auf GitHub\footnote{GitHub ist ein 
Portal zum 
kollaborativen Arbeiten an öffentlichen und privaten Projekten mittels des
Versionsverwaltungssystems \textit{git}\cite{git,github}.} veröffentlicht 
\cite{gh:wonder}. Den Ankerpunkt dieser Bibliothek für Anwendungsentwickler 
bildet die \texttt{Conversation}-Klasse, über die Zugriffe auf das \ac{API} 
erfolgen. Zusätzlich besteht die Notwendigkeit des Zugriffs auf Funktionen 
anderer Klassen, welche z.B. für das Laden und Instanziieren des 
lokalen \ac{IdP} sowie der eigenen Identität zuständig sind. Eine detaillierte 
Beschreibung der Bibliothek mit einer Anleitung, in welcher Reihenfolge die 
einzelnen Aufrufe zu erfolgen haben befindet sich im 
Wiki-Bereich des GitHub-Repositorys\footnote{Ein Repository ist ein 
verwaltetes 
Verzeichnis.}.