\subsection{Überwindung der NAPT-Problematik}
Clients in IT-Umgebungen befinden sich oft hinter einem Router mit einem 
eingebauten \ac{NAPT}-Gateway, welches dafür sorgt, dass IP-Adressen und Ports 
aus dem privaten Adressbereich auf das öffentliche Netz umgesetzt werden 
\cite{rfc2766}. Aus 
Sicht des privaten Netzes stellt dies keinerlei Probleme dar, da abgehende 
Nachrichten direkt adressiert werden können. Kommt jedoch eine Nachricht aus 
dem öffentlichen Netz und wird an einem Client im privaten Netz gesendet, so 
bleibt die 
Infrastruktur hinter dem \ac{NAPT}-Gateway zunächst verborgen und es kann nur 
eine Adressierung an das Gateway erfolgen. Zur Adressierung von Clients hinter 
solch einem Gateway sind Mechanismen notwendig, welche die Clients ausfindig 
machen.

\subsubsection{\acs{STUN}}
\label{sec:stun}
Mit Hilfe des \ac{STUN}-Protokolls, welches von der \ac{IETF} im 
RFC 5389 spezifiziert wurde, ist es privaten 
Multimedia-IP-Systemen im eigenen Netzwerk möglich, die für sie gültigen 
öffentlichen Adressdaten aufzufinden \cite{rfc5389}\cite{trickweber}[S. 291 
ff.].
Dazu werden Nachrichten (\textit{\ac{STUN}-Messages}) zwischen dem 
\textit{\ac{STUN}-Client} und dem 
\textit{\ac{STUN}-Server} ausgetauscht. Der \ac{STUN}-Client muss dabei in der 
entsprechenden 
Endanwendung (in diesem Fall der \ac{WebRTC}-Client) integriert sein und 
enthält Informationen zu den IP-Adressdaten des im öffentlichen 
IP-Netz befindlichen \ac{STUN}-Servers.
Um sich mit dem Server zu verbinden und öffentliche IP-Sockets\footnote{
Ein IP-Socket bezeichnet hier eine Kombination aus IP-Adresse und Portnummer.}
aufzufinden, 
muss der \ac{STUN}-Client zunächst einen \texttt{Binding Request} an den 
\ac{STUN}-Server senden. Dieser antwortet auf die Anfrage mit einer 
\texttt{Binding Response}, in der im Feld \texttt{MAPPED-ADDRESS} die 
öffentliche IP-Adresse und Port des \ac{NAPT}-Gateways enthalten sind, hinter 
dem sich der \ac{STUN}-Client befindet (\autoref{fig:stun}).
Diese erhaltene Adresse kann anschließend vom Client für die Herstellung der 
Verbindung zu einem anderen Teilnehmer eingesetzt werden.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{pic/stun.eps}
	\caption[Funktionsprinzip: \acs{STUN}]{Funktionsprinzip: \ac{STUN} (vgl. 
	\cite{trickweber}[S. 293])}
	\label{fig:stun}
\end{figure}

Bei Nutzung von \ac{STUN} zur Überwindung von \ac{NAPT}-Gateways ist zu 
beachten, dass eine direkte Abhängigkeit des Gateways von den IP-Adressen und 
Ports öffentlicher Server bestehen kann. Je nach \ac{NAPT}-Typ kann es 
vorkommen, 
dass eine Überwindung durch \ac{STUN} nicht möglich ist, insbesondere bei der 
Verwendung von adressabhängigen oder adress- und portabhängigen 
\ac{NAPT}-Gateways.
Dies führt dazu, dass nicht jeder öffentliche IP-Socket 
gleichermaßen für jeden Client als ein solcher sichtbar ist.
Um diesem Problem entgegenzuwirken, sind weitere Mechanismen zur Überwindung 
notwendig.



\subsubsection{\acs{TURN}}
\label{sec:turn}
\ac{TURN} ist ein von der \ac{IETF} spezifizierter Standard zur Überwindung der 
\ac{NAPT}-Problematik \cite{rfc5766}. Dabei stellt \ac{TURN} einen 
Spezialfall von \ac{STUN} dar, mit dem es möglich ist, jegliche Grundtypen von 
\ac{NAPT}-Gateways zu überwinden.
Um dies zu realisieren, wird der \ac{STUN}-Server um ein \textit{Relay} 
erweitert, welches ihm ermöglicht, Nutzdaten zu 
empfangen und 
weiterzuleiten. Dieser Server stellt dabei eine zentrale Instanz im 
öffentlichen Netz zwischen mehreren, in privaten IP-Netzen befindlichen, 
\ac{STUN}-Clients dar \cite{trickweber}[S. 296 ff.].
Im \textit{\ac{STUN}-Relay-Client} müssen für die Verwendung von 
\ac{TURN}-Informationen 
über den zugehörigen \textit{\ac{STUN}-Relay-Server} vorkonfiguriert sein.
Wie auch bei \ac{STUN} erfolgt der Informationsaustausch über Nachrichten, 
wobei \ac{TURN} ein erweiterten Umfang der \ac{STUN}-Nachrichten nutzt:
\begin{itemize}
	\item \textbf{\texttt{Allocate Request}}: Ist die Aufforderung des Clients 
	an den 
	Server einen öffentlichen IP-Socket auf einer Netzwerkschnittstelle des 
	\ac{STUN}-Servers bereitzustellen.
	\item \textbf{\texttt{Allocate Response}}: Ist die Server-Antwort auf den 
	Allocate 
	Request mit dem entsprechenden IP-Socket.
	\item \textbf{\texttt{Send Indication}}: Ist eine Nachricht vom Client an 
	den Server. Dieser leitet die Nachricht wiederum an den entfernten Host 
	weiter. Die 
	Nachricht enthält die gekapselten Nutzdaten. Die Daten werden 
	solange gekapselt, bis eine \texttt{Set Active Destination 
	Request}-Nachricht gesendet wird.
	\item \textbf{\texttt{Data Indication}}: Enthält die gekapselten 
	Nutzdaten 
	des 
	entfernten Clients und wird an den lokalen Client geschickt, der sich 
	hinter dem \ac{NAPT}-Gateway befindet. Wie auch beim \texttt{Send 
	Indication} werden die Daten solange gekapselt, bis eine \texttt{Set 
	Active Destination Request}-Nachricht gesendet wird.
	\item \textbf{\texttt{Set Active Destination Request}}: Ist eine 
	Nachricht,  vom Client an 
	den Server, zur Freigabe der direkte Nutzdatenkommunikation zwischen 
	den Clients.
	\item \textbf{\texttt{Set Active Destination Response}}: 
	Ist die Serverseitige Bestätigung der empfangenen \texttt{Set Active 
	Destination 
	Request} Nachricht.
\end{itemize}

\autoref{fig:turn} zeigt exemplarisch eine Kommunikation zwischen zwei Clients 
und dem \ac{STUN}-Relay-Server (\ac{TURN}-Server). 

Bei der Umsetzung der Pakete durch den \ac{TURN}-Server entstehen zusätzliche 
Verzögerungen sowie Jitter\footnote{Jitter bezeichnet zeitliche Schwankungen 
der 
Amplituden, Frequenzen, Phasenlage oder Pegel eines Signals.} was dazu führt, 
dass 
sehr hohe 
Anforderungen hinsichtlich der Performance solcher Server gestellt werden, um 
diese Effekte möglichst gering zu halten und annähernd eine 
Echtzeitcharakteristik
zu erwirken.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{pic/turn.eps}
	\caption[Funktionsprinzip: \acs{TURN}]{Funktionsprinzip: \ac{TURN} (vgl. 
	\cite{trickweber}[S. 298])}
	\label{fig:turn}
\end{figure}



\subsubsection{\acs{ICE}}
Wie in den Unterabschnitten \ref{sec:stun} und \ref{sec:turn} beschrieben, 
können diese Verfahren nicht in jedem Fall universell eingesetzt werden. 
Problematisch sind z.B. Infrastrukturen, bei denen sich sowohl Alice 
als auch 
Bob in ihren jeweiligen privaten Netzen hinter einem \ac{NAPT}-Gateway 
befinden.

Die im RFC 5245 spezifizierte Methode \ac{ICE} sorgt für die 
Überwindung der 
\ac{NAPT}-Problematik bei beliebigen Kommunikationsszenarien 
\cite{rfc5245} \cite{trickweber}[S. 300 ff.]. Dabei werden die 
Informationen zur Auffindung der IP-Sockets priorisiert angegeben. Bei diesen 
Verbindungsinformationen können sowohl \ac{VPN}-Verbindungen ins öffentliche 
Netz, 
als auch Mechanismen wie \ac{STUN} und \ac{TURN} genutzt werden.

Um dies zu realisieren, werden zunächst gegenseitig die Kontaktinformationen 
übermittelt und es wird anschließend nach der \textit{best-match}-Methode der 
Verbindungsweg mit der möglichst höchsten beidseitigen Priorisierung 
herausgesucht. Ein sich hinter einem \ac{NAPT}-Gateway 
befindlicher Client muss die Funktionalitäten des \ac{STUN}-Clients und 
-Servers 
implementiert haben, um eine \ac{P2P}-Ermittlung des besten Kontakweges 
durchführen zu können. Zusätzlich sollte der \ac{TURN}-Mechanismus zur 
Ermittlung von alternativen Kontaktwegen
implementiert werden.

Bei \ac{WebRTC} können diese Informationen der \texttt{PeerConnection}
über ein Objekt mitgeteilt werden (siehe \autoref{lst:ice:impl}). Diese 
Informationen werden Browser-intern ausgewertet und zur Suche des bestmöglichen 
Verbindungsweges verwendet. Dabei wird zunächst geprüft, ob eine 
\ac{P2P}-Verbindung zwischen den Teilnehmern ohne die Notwendigkeit von 
\ac{STUN} und \ac{TURN} hergestellt werden kann. Sollte dieser Versuch 
fehlschlagen, wird auf die in dem Array angegebenen \ac{STUN}-Server 
zurückgegriffen. Als letzte Möglichkeit wird auf Grund der höheren Komplexität 
der Umweg über einen \ac{TURN}-Server gewählt \cite{html5rocks:webrtc}.
%\autoref{fig:ice} zeigt exemplarisch eine Verbindung zwischen zwei \ac{SIP} 
%User Agents. Dort werden zunächst die Verbindungsdaten ausgehandelt und es 
%werden die alternativen Verbindungsdaten übergeben.
%\begin{figure}[H]
%	\centering
%	\includegraphics[width=0.9\textwidth]{pic/ice.eps}
%	\caption[Funktionsprinzip: \acs{ICE}]{Funktionsprinzip: \ac{ICE} (vgl. 
%	\cite{trickweber}[S. 301])}
%	\label{fig:ice}
%\end{figure}

\lstinputlisting[language=json, caption={\acs{ICE}-Konfiguration einer
\texttt{PeerConnection}}, 
label=lst:ice:impl]{src/ice/config.js}


\subsubsection{Trickle-\acs{ICE}}
Trickle-\ac{ICE} ist ein zur Zeit noch in Entwicklung befindlicher Mechanismus, 
welcher dazu genutzt werden soll, die bei der \ac{ICE}-Prozedur 
entstehenden Verzögerungen auszugleichen \cite{rfc5245, trickleice}. Solche Verzögerungen 
entstehen beispielsweise bei der Abarbeitung vieler \ac{ICE}-Kandidaten zur Findung 
der Kommunikationsparameter, da diese zunächst alle geprüft werden, bevor das 
\ac{SDP} erstellt wird. 

Die Trickle-\ac{ICE}-Prozedur sorgt dafür, dass 
zunächst eine \ac{SDP}-Nachricht mit lokalen Verbindungsparametern an den 
entfernten Teilnehmer gesendet wird. Dieser kann die empfangene Nachricht 
sofort verarbeiten. Anschließend werden inkrementell weitere 
\ac{SDP}-Nachrichten verschickt, welche anhand der angegebenen \ac{STUN}- oder 
\ac{TURN}-Server generiert werden. Dies bietet den Vorteil, dass der Austausch 
der \ac{ICE}-Kandidaten bereits stattfindet, während die Verbindungswege 
weiterer 
Kandidaten geprüft werden.