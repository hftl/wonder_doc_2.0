\subsection{Analyse}

\subsubsection{Signalisierungsprotokolle}
Bereits in einer frühen Version des \ac{WONDER}v1-Frameworks war das Nachladen 
von Protokollen möglich. Nachfolgend werden diese Protokolle als
\textit{Stub} bzw. \textit{Messaging Stub} bezeichnet.
Messaging Stubs wurden von einem Server mittels eines 
\ac{AJAX}-Requests\footnote{Als \ac{AJAX}-Request wird die Nutzung des 
\texttt{XMLHttpRequest}-Objektes zur Kommunikation mit serverseitigen Scripten 
bezeichnet\cite{w3c:xmlhttprequest}. \ac{AJAX} ermöglicht es, Information nach 
dem 
Laden einer Website 
abzurufen und durch \ac{DOM}-Manipulation hinzuzufügen.} 
in den \ac{HTML}-\ac{DOM}\footnote{Das \acl{DOM} bildet die 
Schnittstelle zwischen \ac{HTML} und JavaScript.} geladen 
\cite{w3c:xml,gh:wonder}.
Das Nachladen geschieht über eine Funktion, die für diesen Zweck konzipiert 
wurde
und in \autoref{lst:loadjsfile} zu sehen ist.

In diesem Listing wird ein \ac{HTML}-Script-Tag erstellt, mit Attributen 
versehen
und anschließend in den Head-Bereich des \ac{DOM}-Baumes geschrieben 
\cite{w3c:html5}(Abschnitt 4.11).
Da der Script-Tag ein \texttt{src}-Element mit einem \ac{URI} enthält,
lädt der Browser automatisch die in dem \ac{URI} hinterlegte JavaScript-Datei 
nach.
Diese Variante des Nachladens hat das Problem, dass das \ac{DOM}-Element 
manipuliert wird,
was zu Namenskonflikten beim Nachladen führen kann \cite{dyn-laden}. Die 
Konflikte
beziehen sich auf den Namen der Funktion, welche nachgeladen wird. Sollte eine
Funktion den gleichen Namen wie eine zweite haben, so wird die zweite Funktion die
erste überschreiben, wenn sie später nachgeladen wird. Messaging Stubs dürften demnach
nicht den gleichen Namen besitzen, weshalb für das \ac{WONDER}v1-Framework eine
Standardisierung für die Namensvergabe notwendig ist.
Des Weiteren werden nachgeladene JavaScript-Dateien nicht indiziert,
wodurch sie später nicht gelöscht werden können, da keine Referenz auf sie 
vorhanden ist.
Dies erhöht die Speicherauslastung des Frameworks, je mehr 
Teilnehmerverbindungen hergestellt werden.

\lstinputlisting[language=javascript, caption={Nachladefunktion in 
\ac{WONDER}v1}, 
label=lst:loadjsfile]{src/wonder1/loadjsfile.js}

In \ac{WONDER}v1 wurde ebenfalls ein Timer benötigt, welcher prüft, ob ein 
Messaging Stub
zu einer gegebenen Zeit nachgeladen wurde. Der Aufruf und die Funktion sind in 
\autoref{lst:callloadjsfile} zu sehen.

\lstinputlisting[language=javascript, caption={Aufruf der Nachladefunktion von 
Dateien in \ac{WONDER}v1}, 
label=lst:callloadjsfile]{src/wonder1/callloadjsfile.js}

Die Zeilen 1-16 zeigen die Definition der Prüffunktion und die Zeilen 17 und 18 
die Ausführung der
Nachladefunktion mit anschließendem Aufruf des Timers. Der Timer in Zeile 18 
wartet
zuerst $100 \ ms$ und ruft danach die \texttt{check}-Funktion auf. Sollte der 
Stub bis zu diesem
Zeitpunkt noch nicht nachgeladen worden sein, so wird sich die 
\texttt{check}-Funktion nach
$500 \ ms$ bis zu 20 mal selbst aufrufen. Das entspricht einer maximalen Zeit 
von 
$10,1$ Sekunden\footnote{Die Zeit für die Ausführung der Funktion wurde an 
dieser Stelle nicht 
berücksichtigt, da diese zu gering ist und daher vernachlässigt werden kann.},
in der keine andere Aufgabe abgearbeitet werden kann, da ein Timeout den gesamten Ablauf
des Frameworks unterbricht. Der kritische Teil der Unterbrechung ist jedoch das $500 \ ms$
Warten. Sollte die Funktion kurz nach dem Überprüfen nachgeladen worden sein, so
wird etwa $500 \ ms$ umsonst gewartet. 
An dieser Stelle zeigt sich Optimierungspotential
hinsichtlich der Unterbrechung der Abarbeitung. Im Fokus steht das mögliche 
Abarbeiten
von anderen Funktionen während des Wartens und der schnelleren Überprüfung des Vorhandenseins,
so dass nachfolgende sequentielle Schritte eher ausgeführt werden können.

In Zeile 2 wird der Name des nachgeladenen Stubs dazu verwendet, das Vorhandensein
des Stubs im globalen \texttt{window}-Objekt zu prüfen. Da an dieser Stelle der 
Name 
statt
eines eindeutigen Identifizierers genutzt wird, kann ein falscher Stub geprüft 
werden.
Dies würde auftreten, wenn ein zweiter Stub den gleichen Namen wie ein bereits 
geladener
Stub besitzt, wodurch die Prüfung korrekt verlaufen und die Abarbeitung fortgesetzt würde.
Das Framework würde nun den bereits geladenen Stub für einen neuen Signalisierungsserver
nutzen, wodurch das falsche Protokoll verwendet werden könnte. Im späteren Verlauf,
wenn das Nachladen des Stubs abgeschlossen ist, würde dieser den bisher verwendeten
überschreiben. Sollte das Framework den alten Stub nutzen wollen, so würde es ebenfalls
den falschen verwenden, da der alte Stub nicht mehr existiert. Bei dieser Abarbeitung
zeigt sich ebenfalls Optimierungsbedarf hinsichtlich der Identifizierung der 
Messaging Stubs,
sodass keine Konflikte beim Zugriff auf die Stubs entstehen.

In einer späteren Version des \ac{WONDER}v1-Frameworks wurden diese beiden 
Optimierungspunkte
erkannt und durch die Nutzung von \textit{Require.js} 
verbessert\cite{requirejs}. Das 
Require.js-Framework ermöglicht es,
JavaScript-Dateien nachzuladen und bei Erfolg eine Callbackfunktion auszuführen.
Dadurch kann sichergestellt werden, dass die Existenzprüfung des Nachladens 
einer Datei automatisch erfolgt ist. \autoref{lst:requireaufruf} zeigt die 
Nutzung von Require.js exemplarisch.

\lstinputlisting[language=javascript, caption={Nachladen von Dateien mittels 
Require.js}, 
label=lst:requireaufruf]{src/requirejs/requireaufruf.js}

Die Nutzung von Require.js bietet den Vorteil, dass angeforderte Dateien 
unabhängig 
von ihren Namen geladen werden. Sollten somit Stubs mit dem gleichen Namen 
geladen werden, so überschreiben diese sich nicht gegenseitig und können 
unabhängig voneinander genutzt werden. Die Identifizierung der angeforderten 
Dateien durch Require.js erfolgt dabei über den Herkunftsort der Datei. 
Weiterhin sorgt Require.js dafür, dass angeforderte Dateien im Browser 
\textit{gecached}\footnote{Cachen bezeichnet das Zwischenspeichern von Informationen.} 
werden. Somit müssen diese bei erneuter Anforderung nicht 
über das Netzwerk bezogen werden.


\subsubsection{Frameworkmodule}

In \ac{WONDER}v1 müssen alle zum Framework gehörenden Dateien beim Laden der
Hauptdatei vorhanden sein, da diese,
welche die Schnittstelle zum Implementierer darstellt, auf alle Dateien
Zugriff haben muss. Dies kommt daher, dass keine definierten Abhängigkeiten
der Hauptdatei bzw. der Schnittstelle mit anderen Dateien spezifiziert wurden
und sich Funktionen zu jeder Zeit gegenseitig aufrufen können. Ein weiterer
Grund ist die fehlende Nachladefunktionalität von Teilen des Frameworks.

Der Abhängigkeitsgraph\footnote{Der Graph wurde mit dem Programm
''Graphviz - Graph Visualization Software'' erstellt, welchem lediglich
Abhängigkeiten übergeben werden, woraufhin das
Tool einen Graphen zurückliefert.} von \ac{WONDER}v1 ist in \autoref{gv:w1} zu 
sehen.

\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth]{gv/w1.eps}
	\caption[Abhängigkeitsgraph \acs{WONDER}v1]{Abhängigkeiten und
	gegenseitiger Aufruf der Funktionsprototypen des
	\ac{WONDER}v1-Frameworks}
	\label{gv:w1}
\end{figure}

In der Abbildung wird deutlich, dass ein \texttt{MessagingStub} auf eine
\texttt{Identity}, einen \texttt{Idp} sowie einen \texttt{Participant}
zugreift, also von diesen Dateien abhängt. Das
bedeutet, dass ein Entwickler eines Messaging Stubs Kenntnisse über die interne
Funktionalität des Frameworks haben muss, da sonst der Messaging Stub nicht
korrekt funktionieren kann. Dieses Verhalten ist kontraproduktiv, da es
externen Entwicklern neuer Signalisierungsprotokolle die Erstellung dieser
erschwert.
In der Abbildung wurde der Zusammenhang durch den Frontendentwickler vernachlässigt,
jedoch muss auch dieser Kenntnisse von frameworkinternen Funktionen besitzen, um
\ac{WONDER}v1 implementieren zu können.
In \ac{WONDER}v2 ist es deshalb sinnvoll, externen Entwicklern des
Frontends sowie von Messaging Stubs zu ermöglichen, das Framework mit einer fest
definierten Schnittstelle nutzen zu können und veränderliche interne Funktionen
dabei nicht berücksichtigen zu müssen.
