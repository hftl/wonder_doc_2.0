\subsection{Realisierung}
\label{nachladen:realisierung}
\subsubsection{Signalisierungsprotokolle}
\label{nachladen:realisierung:proto}
Bei der Umsetzung des zuvor festgelegten Konzeptes musste eine 
Anpassung der \texttt{connect}-Funktion vorgenommen werden.
Die Änderung im Messaging Stub ist in 
den Listings \ref{lst:stubs:nodeConnectOld} und \ref{lst:stubs:nodeConnectNew} 
dargestellt.

\lstinputlisting[language=javascript, caption={[Connect-Funktion des Messaging 
Stub in \acs{WONDER}v1]Die 
\texttt{connect}-Funktion im Messaging Stub bei Nutzung von \ac{WONDER}v1}, 
label=lst:stubs:nodeConnectOld]{src/stubs/ms_nodejs_connect_old.js}

\lstinputlisting[language=javascript, caption={[Connect-Funktion des Messaging 
Stub in \acs{WONDER}v2]Die 
\texttt{connect}-Funktion im Messaging Stub bei Nutzung von \ac{WONDER}v2}, 
label=lst:stubs:nodeConnectNew]{src/stubs/ms_nodejs_connect_new.js}

Die Zeile 1 zeigt in beiden Listings die spezifische 
Moduldefinition,
um JavaScript-Dateien mit diesem Framework nachladen zu können. Im neuen Stub in
\autoref{lst:stubs:nodeConnectNew} fällt auf, dass der Name des Stubs
nicht mehr durch das Modul an  Require.js übergeben wird.
Der Grund dafür ist, dass festgestellt wurde,
dass eine Verwaltung der Stubs ohne die Namensübergabe an Require.js den Vorteil bringt,
dass Stubs gleiche Namen haben dürfen und eine Identifizierung auch mit der URI
erreicht werden kann.
Ebenso wurde festgestellt, dass das Zurückliefern einer Instanz ausreicht,
um diese mehrfach verwenden zu können.
Die Rückgabe der Instanz erfolgt beim alten Stub in Zeile 20 und
beim neuen Stub in Zeile 18. Das erneute Instanziieren eines Stubs kann über
seine alte Instanz mittels \texttt{<instanz>.constructor} erfolgen, so dass
der Konstruktor die Klasse neu erstellt.


In der Definition des Messaging Stubs in der 2. Zeile in beiden Listings ist
ersichtlich, dass der neue Stub nun mit Hilfe von \ac{ES6}-Klassen erstellt 
wurde, da diese auch im \ac{WONDER}v2-Framework verwendet wurden. Die 
Verwendung von \ac{ES6}-Klassen ist 
jedoch nicht zwingend notwendig,
da auch eine prototypische Definition wie mit \ac{ES5} möglich ist.

Die Zeile 5 im neuen Stub unterscheidet sich von Zeile 4 im alten Stub 
dahingehend, dass Stubs ihren Signalisierungsserver
nicht mehr fest verankert haben oder diesen wie in
\autoref{lst:stubs:nodeConnectOld} statisch aus der Require.js-Konfiguration 
holen,
sondern die Adresse des Servers erst beim Verbinden zu einem solchen eingetragen wird.
Mit der Methode in \autoref{lst:stubs:nodeConnectOld} besteht das Problem,
dass sich das \ac{WONDER}v1-Framework nur mit einem einzigen 
Signalisierungsserver
verbinden kann, da jeder Stub den gleichen Inhalt aus
\texttt{module.config().connectURL} ausliest.

Eine wesentliche Vereinfachung stellen die Zeilen 13 bis 16 in 
\autoref{lst:stubs:nodeConnectOld} dar, welche durch die 
Zeilen 
6 und 14 in \autoref{lst:stubs:nodeConnectNew} ersetzt wurden. Das Weiterleiten
einer eingehenden Nachricht vom Signalisierungsserver zum \ac{WONDER}-Framework
wurde in \autoref{lst:stubs:nodeConnectOld} über verschachtelte Funktionsaufrufe,
welche in Framework-interne Funktionen eingreifen, ausgeführt. Dies ist 
ersichtlich
an \texttt{Idp.getInstance().createIdentity(...)}. 
Die Methoden des localIdp sollten den Stubs nicht bekannt sein müssen, 
da diese Funktion nur für interne Belange
relevant ist. Das Problem wurde damit gelöst, dass das \ac{WONDER}v2-Framework 
seine
aufzurufende Methode innerhalb des Stubs registriert und dieser lediglich
\texttt{that.onMessage(message)} ausführen muss.



\subsubsection{Frameworkmodule}
\label{nachladen:realisierung:module}
Der Abhängigkeitsgraph von \ac{WONDER}v2 ist in \autoref{gv:w2} zu sehen.
Hierbei sei zu beachten, dass Klassen mit einem großen Buchstaben beginnen
und Funktionsmodule einen kleinen Anfangsbuchstaben besitzen.
Bei dem Vergleich der Graphen von \ac{WONDER}v1 und \ac{WONDER}v2 wurden
nur die relevanten Abhängigkeiten berücksichtigt, um
die Übersichtlichkeit zu wahren. Es wurde bei beiden Graphen nicht auf
Vollständigkeit geachtet, da ein vollständiges Nachvollziehen beider
Frameworkversionen über den Rahmen eines Vergleiches hinaus gehen würde.
Um einen Vergleich an dieser Stelle zu ermöglichen wurden bei beiden
Frameworkversionen die gleichen Anwendungsfälle, wie z.B. der
Aufbau eines Videoanrufs, getestet.

\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth]{gv/w2.eps}
	\caption[Abhängigkeitsgraph \acs{WONDER}v2]{Abhängigkeiten und
	gegenseitiger Aufruf der Funktionen und Klassen des
	\ac{WONDER}v2-Frameworks}
	\label{gv:w2}
\end{figure}

Im Graph ist ersichtlich, dass die \texttt{\ac{WONDER}}-Klasse als zentraler 
Punkt
des Frameworks auftritt und auf die anderen Klassen zugreift. Es wird
ebenso deutlich, dass die \texttt{Conversation}-Klasse eine zentrale Rolle 
innerhalb
des Frameworks darstellt, da diese eine Instanz einer Konversation
ist und alle konversationsspezifischen Module referenziert.
Im Vergleich zu \ac{WONDER}v1 hat \ac{WONDER}v2 sechs zusätzliche Klassen, was
der in der Konzeption erklärten Minimalisierung und
Auftrennung von Funktionsblöcken entspricht.
Ebenso ist der Abhängigkeitsbaum breiter, was durch die Zentralisierung
der Schnittstelle zum Framework-Implementierer und durch die möglichst
direkte Referenzierung zu anderen Klassen entstand.
Weiterhin ist festzustellen, dass im Vergleich zu \autoref{gv:w1}
weniger entgegengesetzte Abhängigkeiten vorhanden sind. Dies konnte
durch die Auflösung von Aufrufketten erreicht werden
sowie durch die Platzierung von Callback-Funktionalitäten in übergeordneten
Klassen.


Auffällig ist, dass in der Grafik der Messaging Stub
an mehreren Stellen genutzt wird, worauf der 
\autoref{signalisierung:konzeption} eingeht.
