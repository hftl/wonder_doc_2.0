\subsection{Konzeption}

\subsubsection{Signalisierungsprotokolle}
\label{nachladen:sigproto}
Da jeder Signalisierungsserver oder ein Signalisierungsserververbund
zu einer Domäne gehört, welche diese verwaltet,
muss das Protokoll, welches mit den jeweiligen Servern der Domäne
kommunizieren kann, aus dieser geladen werden.
Der nachgeladene Messaging Stub stellt dabei die Schnittstelle 
zwischen dem \ac{WONDER}v2-Framework und der
Signalisierungsdomäne dar.

Um mit einem beliebigen Stub beliebige Signalisierungsserver als
Eintrittspunkt in die Signalisierungsdomäne nutzen zu können,
sollte dieser seinen Signalisierungsserver beim Nachladen noch nicht
kennen. Das Wissen, welchen Signalisierungsserver der Stub ansprechen darf,
muss deshalb von anderer Stelle kommen.
Die Autoren haben sich dazu
entschieden, diese Informationsfindung in den \ac{IdP} auszulagern,
welcher beim Login in eine Domäne die jeweiligen Benutzerinformationen
lädt. Da der Betreiber der Domäne seine Signalisierungsserver kennt,
kann er diese Information in den nutzerspezifischen  oder domänenspezifischen
Webfinger Eintrag bzw. seines eigenen Identitätsmanagement-Systems hinterlegen 
(Detaillierte Informationen zu Webfinger befinden 
sich im \autoref{sec:webfinger}).
Das \ac{WONDER}v2-Framework muss anschließend den hinterlegten
Signalisierungsserver-\ac{URI} aus dem Webfinger Eintrag extrahieren und
in den Stub schreiben, sodass dieser seinen Server ansprechen kann.

Die Grundstruktur eines Stubs muss verschiedene Eigenschaften erfüllen:
\begin{itemize}
\item Kompatibilität zu beliebigen Signalisierungsservern
\item Kompatibilität zu dem \ac{WONDER}-Framework
\item Kompatibilität zu beliebigen Signalisierungsprotokollen
\end{itemize}

Da alle Signalisierungsprotokolle unterschiedlich sind, sollten sich Stubs
auf eine Grundfunktionalität beschränken, welche jedes Signalisierungsprotokoll
unterstützt oder emulieren kann. Als kleinsten gemeinsamen Nenner aller zu
unterstützenden Signalisierungsprotokolle definieren die Autoren die folgenden
Funktionalitäten:

Stubs müssen ...
\begin{itemize}
\item sich zu Signalisierungsservern verbinden können.
\item die Verbindung zu Signalisierungsservern trennen können.
\item Nachrichten zu einem Messaging Server und zu \ac{WONDER}v2 senden 
können.
\item Nachrichten von einem Messaging Server und von \ac{WONDER}v2 empfangen 
können.
\item empfangene Nachrichten konvertieren können. 
\end{itemize}

\autoref{fig:wonderv2msgExchange} verdeutlicht das Konzept des 
Nachrichtenaustauschs zwischen dem Framework und einem 
Messaging 
Server.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{pic/wonderv2_message_convert.eps}
	\caption[Nachrichtenaustausch zwischen \acs{WONDER}v2 und einem 
	Messaging Server]{Austausch von 
	Nachrichten zwischen dem \ac{WONDER}v2-Framework und 
	einem Messaging Server über den Messaging Stub}
	\label{fig:wonderv2msgExchange}
\end{figure}

Diese sehr reduzierte Grundfunktionalität deckt nach Meinung der Autoren
alles Nötige ab, um mit Servern im Web-Umfeld kommunizieren zu können.
Mit diesem Funktionsumfang ist es nicht nur möglich, einen Signalisierungsserver
mittels Websocket-Verbindung nutzen zu können, sondern ebenfalls, eigene 
Lösungen zu verwenden,
wie das Abrufen und Senden von Nachrichten mittels 
\ac{HTTP}1.1-Long-Polling\footnote{Beim Long-Polling fragt ein Client bei einem 
Server neue Informationen an. Der Server hält dabei den Request solange 
offen, bis entsprechende Daten verfügbar sind und sendet diese an den Client. 
Nachdem der Client die Daten empfangen hat, sendet dieser einen 
Request an den Server und der Vorgang wiederholt sich. Somit kann das Verhalten 
von Server-Push-Events emuliert werden.} 
und \ac{REST}\footnote{\ac{REST} bezeichnet einen
Architekturstil zur Realisierung von 
Webservices.}-Abfragen\cite{rfc6455,rfc6202}.
Die Autoren empfehlen jedoch keine eigenen Lösungen, sondern die Verwendung der 
bereits
vorhandenen Kommunikationsmöglichkeit über Websockets, welche eingebaute
Funktionalitäten wie einen Keep-Alive-Mechanismus\footnote{Dieser Mechanismus 
sorgt dafür, dass eine Verbindung zu einem Server durch das Senden von 
\textit{Keep-Alive}-Nachrichten kontinuierlich aufrecht erhalten wird.} mit 
sich bringen.


Das Grundkonzept des \ac{SigOfly}-Mechanismus wurde von den Autoren aus dem 
\ac{WONDER}"-v1-Framework übernommen. Die Änderungen des Konzeptes belaufen 
sich dabei darauf, dass Funktionen innerhalb der Messaging Stubs keine Aufrufe 
auf 
Funktionen des \ac{WONDER}v2-Frameworks haben sollen.
Dies stellt aus Sicht der Autoren eine wesentliche Verbesserung gegenüber dem 
\ac{WONDER}v1-Framework dar, da Entwickler von Messaging Stubs nach 
erfolgreicher 
Umsetzung des Konzeptes kein Wissen über \ac{WONDER}v2-interne Funktionen haben 
müssen, sondern lediglich die Umsetzung der \ac{WONDER}v2-Nachrichten auf 
Signalisierungsnachrichten des zugehörigen Messaging Servers bearbeiten müssen.

Weiterhin wird angestrebt, im Zuge der konsequenten Nutzung eines modularen 
Konzeptes die Stubs so zu designen, dass diese im \ac{AMD}-Format
vorliegen und 
dynamisch nachgeladen und instanziiert werden können \cite{gh:amdjs}.

\subsubsection{Frameworkmodule}

Das Nachladen von Klassen als Module des Frameworks kann auf ähnliche Weise stattfinden,
wie in Abschnitt \ref{nachladen:sigproto} beschrieben wurde. Module müssen dabei
in einzelne Dateien ausgelagert werden, da Require.js nur Dateien als
Ganzes nachladen kann und keine Teile aus diesen. Das Definieren einer Klasse
als \ac{AMD}-Modul ist nicht nötig, da intern genutzten Klassen
als Instanz in \ac{WONDER}v2 auftreten sollen und keinen abgeschlossenen
Geltungsbereich erfordern.

Für nachzuladende Funktionen als Module ist eine Definition
dieser als AMD-Modul vorteilhaft, da anwendungsspezifische Funktionen
nur im Geltungsbereich der Aufgabe bestehen müssen. Die Nutzung
anderer Klassen kann durch eine Funktion und in ihrem Kontext stattfinden.
Dadurch muss auf die nachgeladene Funktion nach ihrer Abarbeitung keine Referenz
im Framework gehalten werden.
Die Funktion kann demnach ihre Gültigkeit im Framework verlieren.

Da nachzuladende Klassen nach Meinung der Autoren, eine bestimmte
kontextspezifische Aufgabe haben, müssen diese klein gehalten werden,
um den Austausch und die Wartung zu vereinfachen. Eine Klasse hat
einen bestimmten Zweck im Framework, wie das Speichern von
Attributen als Datentyp oder das Bereitstellen einer Schnittstelle
zur Erstellung von Nachrichten, weshalb eine Auftrennung der Klassen
in kleine Module möglich ist. Das minimalistische Prinzip ist jedoch
nicht bei Klassen durchführbar, welche Schnittstellen zu Framework-Interna
und gleichzeitig zu externen Entwicklern bereitstellen sowie jene
zur Akkumulation von Aufrufen anderer Klassen. Die \texttt{\ac{WONDER}}-Klasse
selbst stellt eine Klasse dar, auf welche diese Punkte zutreffen.
Da die \texttt{\ac{WONDER}}-Klasse jedoch als Einstiegspunkt für den 
Frameworkimplementierer
genutzt werden soll, demnach initial geladen wird
und kein Modul als solches darstellt, sind die aufgezählten Punkte 
für diese nicht relevant.

Die \texttt{\ac{WONDER}}-Klasse sowie die \ac{WONDER}v2-Klassen,
welche als Module nachgeladen werden können,
sind in \autoref{gv:w2} in Abschnitt \ref{nachladen:realisierung:module} 
dargestellt.

