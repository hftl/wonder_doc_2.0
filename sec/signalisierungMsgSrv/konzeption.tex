\subsection{Konzeption}
\label{signalisierung:konzeption}
\subsubsection{Empfang von Nachrichten}
Um Nachrichten von anderen Teilnehmern empfangen zu können,
muss sich das Framework mit einem Messaging Server verbinden.
Die Information, welcher Signalisierungsserver verwendet wird,
sollte nicht in dem nachgeladenen Messaging Stub stehen, da
der Signalisierungsserver der Domäne wechseln kann. Einen 
anderen Server zu nutzen, kann notwendig sein,
wenn Wartungsarbeiten vorgenommen werden müssen oder ein Server
seinen Dienst durch einen Fehler beendet. Die Adresse des Servers
sollte deshalb von der jeweiligen Identität extrahiert werden, mit
welcher sich ein Nutzer an einer Domäne anmeldet. Alternativ könnte
diese Information auch von der Webseite kommen, welche ein Nutzer
aufruft. Diese Variante hätte jedoch den Nachteil, dass eine Webseite keine
Anmeldung an verschiedenen Domänen unterstützen würde.

Nachdem das Framework die Adresse des Signalisierungsservers abgerufen hat 
(\autoref{a:wonderv2flow} -- Schritt 2),
muss es sich den Stub holen (\autoref{a:wonderv2flow} -- Schritt 3), welcher 
sich 
mit dem zugehörigen Server verbindet (\autoref{a:wonderv2flow} -- Schritt 4), um
Nachrichten empfangen zu können, wenn ein anderer Teilnehmer eine
Kommunikation beginnen möchte. Da die 
Verbindung zu einem Messaging Server
zu diesem Zeitpunkt noch keinen Konversationskontext besitzt, sollte
die Verbindung zu diesem nicht in einer Konversation gespeichert werden,
sondern in einer anderen Instanz. Die Autoren haben sich dazu entschlossen,
die Verbindung einer Identität im \ac{WONDER}v2-Framework zuzuordnen, da diese
ein Endpunkt für
einen Signalisierungsserver darstellt und ein Server die Identität als
Schlüssel zu einer Verbindung zuordnen kann. Weiterhin haben sich die Autoren 
entschlossen, diesem speziellen Fall, bei dem eine Identität nicht
in einer Konversation vorhanden ist, eine Instanz zuzuweisen. Diese bearbeitet 
den Empfang von \texttt{Invitation}-Nachrichten und nimmt die Zuweisung
der Identität zu einem Teilnehmer (\texttt{Participant}-Klasse) sowie einer
Konversation (\texttt{Conversation}-Klasse)
vor. Die bearbeitende Instanz wird \textit{Invitation Handler} genannt.

\subsubsection{Austausch von Nachrichten}
Als Inhalt von Nachrichten zwischen \ac{WONDER}-Frameworks
werden immer \ac{WONDER}-Messages genutzt,
sodass zu jeder Zeit ein einheitliches Format übertragen wird.
Der Austausch von Signalisierungsnachrichten zu einem Server erfolgt
dabei immer zwischen den Messaging Stubs der \ac{WONDER}-Frameworks,
wodurch ein abgeschlossenes System entstehen soll,
welches eine Fehlervermeidung bei der Signalisierung erhöht.
Die einzige externe Fehlerquelle im Framework stellt dabei der Messaging Stub an sich dar,
welcher jedoch ebenfalls eine geringe Fehlerwahrscheinlichkeit haben sollte,
da der Entwickler eines Stubs ausschließlich zwischen Instanzen seiner Stubs Nachrichten
austauscht und demnach seinen eigenen Code kennt und Sende- sowie
Empfangsmethoden aufeinander abstimmen kann.

Die übertragenen \ac{WONDER}v2-Nachrichten werden im \ac{WONDER}v2-Framework
von mehreren Klassen generiert. Die \texttt{RtcEvtHandler}-Klasse erzeugt 
Nachrichten
betreffend der \texttt{PeerConnection}, während das \texttt{callSingle}-Modul 
sowie das \texttt{dataChannel}-Modul
\texttt{In"-vitation}-Nachrichten generiert. Empfangen werden diese in jedem Fall
von dem \texttt{Msg"-EvtHandler}, welcher diese vom Messaging Stub erhält und 
anschließend verarbeitet.
Ein besonderer Fall einer \ac{WONDER}-Nachricht ist die 
\texttt{Invitation}-Nachricht,
deren Verhalten kurz erläutert wird, da diese anders als bei
den übrigen Nachrichtentypen eine konzeptionelle Eigenart aufweist.

Im Fall einer \texttt{Invitation}-Nachricht wird der Invitation Handler
eine Konversation in der \ac{WONDER}v2-Klasse sowie die zugehörigen Teilnehmer
erstellen. Des Weiteren wird der Invitation Handler in nachfolgenden Schritten
als \textit{MessageEventHandler} (\textit{MsgEvtHandler}) fungieren, da dieser 
eine Instanz eines
MsgEvtHandlers ist und demnach keine eigene Klasse benötigt.
Dieser designtechnische Schritt ist vorteilhaft, um die Größe des
Quelltextes minimal zu halten sowie Kopien von Attributen zu vermeiden.
Die damit einhergehenden Fehlerpotentiale durch Instanzreferenzierungen
und Kontextauftrennung einer bereits im Kontext stehenden Klasse
wird dadurch vermindert.


\subsubsection{Nachrichtentypen}

Die Autoren sind der Meinung, dass die Nachrichtentypen des \ac{WONDER}v1-
Frameworks größtenteils für \ac{WONDER}v2 übernommen werden, jedoch
auch einige der Nachrichtentypen geändert oder verworfen werden
sollten. Nachrichten zum Senden einer Einladung und zum Akzeptieren von 
Einladungen
sind integrale Bestandteile des Frameworks und müssen vorhanden sein.
Nachrichten wie jene zum Offerieren einer Rolle in einer Konversation
und solche zum Abfragen einer Datenbank sind laut Meinung der Autoren
zunächst nicht nötig. Um dem Anspruch gerecht zu werden, das 
\ac{WONDER}v2-Framework
auf die Grundfunktionalitäten zu beschränken und so übersichtlich wie
möglich zu halten, müssen deshalb alle nicht genutzten Nachrichten
entfernt und gegebenenfalls Funktionalitäten zusammengefasst werden.

Die möglichen Nachrichtentypen sind im \autoref{a:wonderv2msg} zu
finden und gegenüber \ac{WONDER}v1 reduziert worden.





\subsubsection{Multiparty-Calls}
Durch die Verwendung des modularen Konzeptes wurde durch die Autoren von Beginn 
an der Fokus darauf gelegt, dass die Anwendung möglichst skalierbar bleibt. So 
wurde von den
Autoren bereits bei der Konzeption berücksichtigt, dass 
das zukünftige \ac{WONDER}v2-Framework die Möglichkeit haben soll,
\textit{Multi-Party-Konversationen}\footnote{Als Multi-Party-Konversation 
wird eine Konversation mit mindestens drei Teilnehmern bezeichnet.} 
zu verwalten. Ein Teil des entwickelten Codes wurde 
diesbezüglich bereits in ein Modul \texttt{callSingle} ausgelagert. Ebenso 
existiert die Datei \texttt{callMultiple} bereits als Skeleton\footnote{Ein 
Skeleton bezeichnet ein Grundgerüst, welches den groben Rahmen für die 
Entwicklung vorgibt.}.

Zur zukünftigen Entwicklung von Multi-Party-Funktionalitäten gibt es zwei 
grundlegend verschiedene Ansätze für den Nutzdatenaustausch, welche in 
\autoref{fig:multiparty:architectures} dargestellt sind.

\begin{figure}[H]
	\centering
	\begin{subfigure}[b]{0.43\textwidth}
		\includegraphics[width=1\textwidth]{pic/fully-meshed.eps}
		\caption{Vollvermaschte Architektur mit $n=4$ \newline}
		\label{fig:mutliparty:meshed}
	\end{subfigure}
	\quad % spacing: \quad, \qquad, \hfill
	\begin{subfigure}[b]{0.43\textwidth}
		\includegraphics[width=\textwidth]{pic/mcu.eps}
		\caption{Sternförmige Architektur mit \acs{MCU}\newline  mit $n=4$}
		\label{fig:multiparty:mcu}
	\end{subfigure}
	\caption[Multi-Party-Topologien]{Multi-Party-Topologien; $n$ stellt die 
	Anzahl verbundener Endgeräte dar}
	\label{fig:multiparty:architectures}
\end{figure}

Wie man aus \autoref{tab:multiparty:vgl} entnehmen kann, bieten beide Formen 
der Architekturen Vor- und Nachteile hinsichtlich des Nutzdatenaustauschs.
An dieser Stelle kann keine konkrete Aussage darüber getroffen 
werden, welche der beiden Architekturformen besser geeignet ist, da dies 
maßgeblich von 
der Anzahl der verbundenen Clients abhängt. Es zeichnet sich jedoch 
ab, dass bei einer vollvermaschten Architektur die insgesamt benötigten
Verbindungen  quadratisch ansteigende Komplexität besitzen sowie die
Verbindungsanzahl pro Teilnehmer linear ansteigt.
Clients können demnach bei einer vollvermaschten Architektur eher an ihre
Grenzen stoßen und mit weniger Teilnehmern gleichzeitig eine Verbindung
aufbauen, als es
mit einer performanten \ac{MCU} der Fall wäre\cite{bigO}. Des Weiteren müsste 
ein Domänen-Betreiber
nur die Leistungsfähigkeit der \ac{MCU} sicherstellen und könnte die variierende
Leistung verschiedener Clients vernachlässigen. Dies ist jedoch nur der Fall,
wenn Video- respektive Audioströme in der MCU gemultiplext\footnote{
Multiplexen bezeichnet das Zusammenfassen mehrerer Signale, welche
gebündelt über ein Medium übertragen werden.} werden.

Die Architektur lässt sich in ähnlicher Weise auf Signalisierungsnachrichten
anwenden. Die
Komplexitätsbetrachtung ist gleich, da bei der Verwendung mehrerer
Signalisierungsserver ebenso viele Verbindungen von Clients zu Servern 
gehalten werden müssen.
Zusätzlich ist bei der vollvermaschten Architektur der Nachrichtenaustausch
zwischen Messaging Servern oder das Instanziieren mehrerer Messaging Stubs
im Client nötig, weshalb dafür eine komplexere
Lösung von Nöten ist, als es mit der Verwendung eines Servers der Fall wäre.
Für Multi-Party-Konversationen ist aus den genannten Gründen
die Verwendung eines zentralen Servers besser geeignet.


\autoref{tab:multiparty:vgl} zeigt die Komplexitätsbetrachtung
im Vergleich. In dieser Tabelle und nachfolgend wird die
Nutzdatenübertragung weiter erläutert.

\begin{table}[H]
\centering
\begin{tabular}{p{2.8cm}|p{5.5cm}|p{6.3cm}}
   & Vollvermaschte Architektur & Sternförmige Architektur 
   mit 
   \ac{MCU} \\
 \hline
 Beschreibung & Jeder Client baut zu allen Teilnehmern der Konversation eine 
 eigene Verbindung auf. & Teilnehmer verbinden sich mit der \ac{MCU}, welche 
 die Verbindungsdaten an die entsprechenden Clients weiterleitet.  \\
 Anzahl nötiger \newline Verbindungen 
 & \begin{tabular}{p{2.5cm}|r} 
 $ C = \frac{n \cdot (n-1)}{2} $ & $\mathcal O(n^2)$ 
 \end{tabular} 
 & \begin{tabular}{p{2.8cm}|r}
 $ C = n $ & $\mathcal O(n)$
 \end{tabular} \\
 Verbindungen \newline je Client 
 & \begin{tabular}{p{2.5cm}|r} 
 $ c = n-1 $ & $\mathcal O(n)$
 \end{tabular} 
 & \begin{tabular}{p{2.8cm}|r} 
  $ c = 1 $ & $\mathcal O(1)$
  \end{tabular} \\
 Vorteile & -- Kein zusätzlicher Server  & -- Geringe Auslastung der Clients \\ 
 Nachteile & -- Hohe Belastung für Clients & -- Zusätzlicher Server 
 \newline 
 -- \ac{SPOF}
\end{tabular}
\caption{Gegenüberstellung von Multi-Party-Architekturen}
\label{tab:multiparty:vgl}
\end{table}

Da bei einer Verbindung von drei Clients unabhängig von der 
Architektur auch nur drei Verbindungen benötigt werden, sollte in diesem Fall 
die vollvermaschte Variante bevorzugt werden. Ab 
einer Verbindung von vier Clients steigt die Anzahl der Verbindungen bei einer 
Vollvermaschung stark an. Zudem müsste jeder Client $n$ Verbindungen halten, 
was zu einem erheblichen Anstieg der Rechenauslastung der Clients führen würde. 
In diesem Fall sollte auf das Konzept der sternförmigen Architektur 
zurückgegriffen werden, da lediglich eine Verbindung zur 
\ac{MCU} aufgebaut werden muss. Dabei steigt die Anzahl der 
Verbindungen, welche von der \ac{MCU} verarbeitet werden müssen, linear an, was 
insbesondere bei sehr vielen Verbindungen zu einer hohen Auslastung dieser 
führen kann. Weiterhin muss erwähnt werden, dass im Falle der Nutzung des 
sternförmigen Konzeptes die \ac{MCU} im besten Fall redundant ausgelegt werden 
sollte, um dem Negativaspekt des \ac{SPOF} entgegenzuwirken.