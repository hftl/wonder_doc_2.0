\subsection{Schnittstelle für Anwendungsentwickler}
Um die Integration des \ac{WONDER}v2-Frameworks für Anwendungsentwickler so 
einfach wie möglich zu gestalten, soll eine einheitliche Schnittstelle 
geschaffen werden, über die mittels einfacher Funktionsaufrufe auf das 
Framework zugegriffen werden kann.
Des Weiteren soll die Einbindung des Frameworks in bereits bestehende Strukturen
so einfach wie möglich gemacht werden. 
Die dabei festgelegten \ac{API}-Funktionen inklusive der Beschreibung der 
erwarteten Parameter sind im \autoref{a:wonderv2api} näher beschrieben.


Weiterhin soll das Framework das dynamische Nachladen von Funktionen 
unterstützen,
um beim Nutzer möglichst kleine Funktionsblöcke zu haben. 
Der Frameworknutzer soll somit nur den anwendungsspezifischen Code vorhalten
müssen und andere Funktionen erst vom Server abrufen, wenn diese benötigt 
werden.
Damit soll die Speicherauslastung, statische Komplexität, das Übertragungsvolumen
und die Übertragungszeit verringert werden.

\subsection{Nutzung unabhängiger Server}
Das Projekt soll alle benötigten Teile auf verschiedenen physikalischen Rechnern
nutzbar machen. Die Server sollen somit getrennt voneinander agieren können,
um einerseits eine Lastverteilung und andererseits eine höhere 
Ausfallsicherheit bzw. Fehlerpropagierung zu erreichen.

Aus diesem Grund haben sich die Autoren für die Aufteilung der 
Funktionalitäten auf nachfolgende Serversysteme entschieden:
\begin{itemize}
	\item \textbf{Webserver}: Liefert die entsprechende Webanwendung 
	aus, 
	welche das \ac{WONDER}v2-Framework implementiert (\autoref{sec:ui})
	\item \textbf{Identity Provider}: Nimmt Anfragen zu Benutzern 
	entgegen und 
	liefert deren Identitäten zurück (\autoref{sec:identitymgmt})
	\item \textbf{Stub Provider}: Stellt Messaging Stubs bereit 
	(\autoref{sec:nachladeproblematik})
	\item \textbf{Messaging Server}: Verarbeitet 
	Signalisierungsnachrichten  
	(\autoref{sec:signMsgServer})
	\item \textbf{Codec Provider}: Stellt Codecs für die Verarbeitung 
	von 
	Nachrichten bereit, welche über den \ac{WebRTC} Datenkanal 
	ausgeliefert 
	werden (\autoref{sec:codecmgmt})
\end{itemize}
Der schematische Zusammenhang der Server wird in 
\autoref{a:wonderv2flow} 
dargestellt, wobei es nicht zwingend notwendig ist, dass Alice und Bob 
unterschiedliche Server nutzen.

Einen weiteren Vorteil der Separierung bildet das Nachladen von 
Messaging 
Stubs, welche mit
diesem Ansatz nicht mehr sofort mit dem Webseitenaufruf geladen werden, sondern
dynamisch in das Framework eingebettet werden können. Logische Funktionalitäten
wie das Identitätsmanagement, die Bereitstellung des \ac{GUI} oder die 
Auslieferung der
nachladbaren Signalisierungsprotokolle sind somit voneinander getrennt. Die 
Separierung
der Server soll damit das modulare Konzept unterstützen.


\subsection{JavaScript-Version}
Im Zuge der ständigen Weiterentwicklung der Programmier- und Scriptsprachen 
beschäftigt sich die 
\ac{ECMA} 
mit der Entwicklung des Sprachkerns von JavaScript\cite{ecma}.  Dabei war 
bis 
Mitte Juni 2015 \ac{ES5} aktuell und 
in den am meisten verbreiteten Browsern 
implementiert\cite{ecmacomptable, ecma262_5_1}.
Im Juni 2015 wurde der \ac{ES6}-Standard veröffentlicht, in dem es einige 
Neuerungen gab\cite{ecma6features,ecma262_6}.
\\
Aktuell ist die Integration des \ac{ES6}-Standards nicht abgeschlossen. Noch 
nicht alle Features sind in den neuesten Versionen komplett integriert worden. 
Um es Entwicklern trotzdem zu ermöglichen, die im Standard beschriebenen 
Features nutzen zu können, gibt es Organisationen, welche 
\textit{Polyfills}\footnote{Ein Codebaustein zur Bereitstellung von 
Features, welche vom Browser nicht unterstützt werden.}
dafür bereitstellen.
\\
Bei der Neukonzeption des Frameworks wurden die Standards in Hinblick auf die 
Funktionalitäten und Implementierung des neu entwickelten Codes 
gegenübergestellt. \autoref{tab:es5es6comp} zeigt eine Bewertungseinschätzung 
der 
beiden 
\ac{ECMA}-Standards im Bezug auf das \ac{WONDER}-Framework.

\begin{savenotes}
\begin{table}[H]
\centering
\begin{tabular}{p{7.5cm}|p{7.6cm}}
 \ac{ECMA}-262 5.1 & \ac{ECMA}-262 6.0 \\
 \hline
 \textcolor{blue}{$\pm$} Klassenrealisierung durch Prototypen\footnote{
 Ein Prototyp ist ein klassenähnliches Objekt, aus welchem Eigenschaften und
 Methoden zur Laufzeit kopiert und verändert werden können \cite{wiki:prototyp}.} & 
 \textcolor{dkgreen}{$+$} Klassenbasiertes Konzept \\
 \textcolor{red}{$- -$} Kompletter Code wird geladen & \textcolor{dkgreen}{$+ 
 +$} 
 Import / Export von Modulen \\
 \textcolor{red}{$- -$} Asynchrone Aufrufe mit Timeouts & 
 \textcolor{dkgreen}{$+ +$} 
  Kapselung asynchroner Operationen\newline durch Promises \\
 \textcolor{dkgreen}{$+ +$} 100\%-ige Kompatibilität & \textcolor{red}{$- -$} 
 Keine 100\%-ige Kompatibilität
\end{tabular}
\caption{Gegenüberstellung der 
Vor- 
und Nachteile von \acs{ES5} und \acs{ES6}}
\label{tab:es5es6comp}
\end{table}
\end{savenotes}

Nach Wichtung aller Faktoren wurde der \ac{ES6} Standard zur Verwendung bei der 
Erstellung des Prototypen ausgewählt, da dieser zukünftig in Browsern Standard 
sein wird. Zudem bietet er viele Features, die für das Framework essentiell 
sind. Um 
dem Fakt entgegenzuwirken, dass einige der Features noch nicht in allen 
Browsern integriert wurden, ist es notwendig, ein Polyfill zu verwenden, 
welches 
die gewünschten Funktionen liefert. 
Weiterhin stellt aus Sicht der Autoren der Fakt, dass der \ac{ES6}-Standard 
noch nicht in allen Browsern in vollem Umfang implementiert ist, kein 
erhebliches Problem dar, da ebenso der \ac{WebRTC}-Standard noch nicht 
finalisiert wurde. Es lässt sich prognostizieren, dass eine ausreichende 
Implementierung des \ac{ES6}-Standards in naher Zukunft erfolgen wird,
da die bisherige Entwicklung schnell voranschreitet und die Hersteller ihre
Browser mit neuen Funktionalitäten zukunftsfähig machen wollen. Somit 
wird voraussichtlich eine volle Kompatibilität zwischen dem Browser  
und dem Framework zur Finalisierung des \ac{WebRTC}-Standards vorhanden sein.

\subsection{Promises und Callbacks}
Da in JavaScript Funktionsblöcke oft asynchron abgearbeitet werden, ist es 
notwendig den Code so zu strukturieren, dass Ereignisse, welche voneinander 
abhängen, sequenziell 
abgearbeitet werden können. Insbesondere bei Operationen, welche Daten über
Netzwerkverbindungen zu entfernten Servern senden oder von diesen empfangen ist 
es notwendig abzuwarten, ob diese Aktionen erfolgreich waren oder im Fehlerfall 
auf 
diesen zu reagieren. Um eine Abarbeitung folgender Funktionsblöcke vor 
Eintreffen eines Ereignisses zu verhindern gibt es verschiedene Möglichkeiten.

Eine Variante zur Serialisierung solcher Operationen ist die Nutzung 
sogenannter \textit{Callback}-Funktionen. Dabei wird beim Aufruf einer Funktion 
eine weitere Funktion dieser als Parameter hinzugefügt und erst ausgeführt, 
wenn eine Abarbeitung des Funktionsblocks erfolgreich war.
Eine weitere Möglichkeit ist die Nutzung von \textit{Promises}, welche mit 
Verabschiedung des \ac{ES6}-Standards 
eingeführt wurden. \autoref{lst:cb} und  \autoref{lst:promise}  stellen die 
Unterschiede der beiden Varianten 
gegenüber.

\begin{minipage}[H]{0.5\textwidth}
	\lstinputlisting[language=javascript, caption={JavaScript Callbacks}, 
label=lst:cb]{src/cb_vs_promise/cb_example.js}
\end{minipage}
\begin{minipage}{0.5\textwidth}
\lstinputlisting[language=javascript, caption={JavaScript Promises}, 
label=lst:promise]{src/cb_vs_promise/promise_example.js}
\end{minipage}

Um eine große Akzeptanz für das \ac{WONDER}v2-Framework zu 
schaffen und \ac{API}-Funk"-tionen möglichst einfach vielen Entwicklern 
zugänglich 
zu 
machen, wurde sich dafür entschieden Promises und Callbacks zu 
verwenden. Somit steht es dem Entwickler frei, welche dieser Methoden 
er verwendet, je nachdem, welcher Programmieransatz ihm besser erscheint
\cite{quora:promiseCallback}.

\subsection{Struktur der Medienanforderung}
In \ac{WONDER}v1 werden Medieneigenschaften (dort als 
\texttt{constraints} bezeichnet) einer Verbindung in der in 
\autoref{lst:w1:constraints} dargestellten Form übermittelt. Diese 
dienen zum 
Herstellen einer \ac{RTC}- bzw. einer Datenverbindung vom lokalen zum 
entfernten Teilnehmer.

\lstinputlisting[language=javascript, caption={Medienanforderung in 
\acs{WONDER}v1}, 
label=lst:w1:constraints]{src/demand/constraints.js}


Das gewählte Format für die Angabe der freizugebenden Medien scheint 
aus Sicht 
der 
Autoren nicht optimal. Das liegt zum einen daran, dass keine 
vereinfachte 
Angabe der Parameter 
gegeben wird und zum anderen ein- und ausgehende Verbindungsparameter 
(\texttt{direction}) aufwendig vom Framework zerlegt werden müssen.

Aus diesem Grund wird an das \ac{WONDER}v2-Konzept die Anforderung 
gestellt, 
verschiedene Methoden der 
Medienanforderung (nachfolgend auch als \textit{Demand}\footnote{Der 
Begriff 
Demand wurde gewählt, da es sich lediglich um einen Wunsch zur Freigabe 
der 
bzw. 
des angegebenen Mediums handelt.} bezeichnet) zu 
unterstützen:
\begin{enumerate}
	\item Ein boolescher Wert sorgt für die Anforderungen aller bzw. keiner der 
	verfügbaren Medien.
	\item Ein String, welcher den Medientypen beinhaltet, sorgt für die 
	Anforderung von diesem.
	\item Ein Array von Strings fordert mehrere Medientypen gleichzeitig an.
	\item Ein Objekt enthält die anzufordernden Medientypen.
	\item Das Objekt beinhaltet zu den Medientypen Unterobjekte mit
	zusätzlichen Anforderungen an das entsprechende Medium.
	\item Es wird ein Objekt mit unterschiedliche Anforderungen hinsichtlich 
	eingehender und ausgehender Medienanforderungen genutzt.
\end{enumerate}
Die  Varianten 1-5 sorgen dabei für eine symmetrische Anforderung der 
freizugebenden Medien, während Variante 6 dafür sorgt, 
dass 
beispielsweise der Sender der Anforderung den Empfänger darum bittet, die 
lokalen Audio- und Videoressourcen freizugeben, während er hingegen 
selbst nur 
eine Audioverbindung freigibt.
Im \autoref{lst:demand:beforeConverted} sind die Varianten 1-6 beispielhaft  
aufgeführt.

\lstinputlisting[language=javascript, caption={Unterstützte 
Anforderungsformate für Medien in \acs{WONDER}v2}, 
label=lst:demand:beforeConverted]{src/demand/beforeConverted.js}

\subsection{Dokumentation}
\label{sec:doc}
Zur Beschreibung des Frameworks sowie der 
Übergabeparameter und Datentypen für die Klassen und Funktionen wurde das 
\textit{Gulp}-Plugin (Informationen zu Gulp: siehe \autoref{sec:auto:real}) des 
Dokumentations-Generators 
ESDoc\cite{esdoc} genutzt.
Dieses Plugin detektiert definierte Tags in Kommentaren des 
\ac{ES6}-konformen JavaScript-Quellcodes. Diese Tags werden 
anschließend von ESDoc verarbeitet und es wird eine Online-Dokumentation 
daraus erstellt.
\autoref{fig:esdoc} zeigt einen exemplarischen Ausschnitt der aus dem 
Quellcode generierten Dokumentation.


\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{pic/esdoc.png}
	\caption{Weboberfläche von ESDoc}
	\label{fig:esdoc}
\end{figure}
