// 1.) Boolean value for demand all/nothing
var demand = true;

// 2.) single string
var demand = "audio";

// 3.) array of strings
var demand = ["audio","video" /* ... */ ];

// 4.) simple objects
var demand = {audio: true, video: true /* ... */ };

// 5.) objects with additional requirements
var demand = {
  video: {
    mandatory: { maxWidth: "1920" /* ... */ },
    optional: [ { minFrameRate: "60"}, /* ... */ ]
  } /* ... */
};

// 6.) object with different incoming/outgoing requirements
var demand = {
  in : {
    video: { /* ... */ },
    audio: true
  },
  out : {
    audio: true
  }
};
