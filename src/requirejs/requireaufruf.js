require(["http://example.org/file.js"],
	function(file) {  // successfully received the file
		console.log("file downloaded:", file);
	},
	function(error) { // download failed
		console.error(error);
	}
);
