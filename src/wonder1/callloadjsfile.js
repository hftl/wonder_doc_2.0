var check = function(stub, callback, count) {
  if (typeof(window[stub]) == "function") {
    var msgStub = new window[stub]();  // instantiate stub
    if (typeof(msgStub) == "object") { // should be an object
      // assign the new msgStub object to
      // the "impl" field of the container stub
      that.msgStub.setImpl(msgStub);
      that.msgStub.message = "stub downloaded from:"+that.msgStubLibUrl;
      callback(that.msgStub); // return container-stub in callback
    }
  } else { // wait if stub is not in window object
    count++;
    if (count < 20) setTimeout(check, 500,  stub, callback, count);
    else callback(); // stop waiting, stub is not available
  }
};
this.loadJSfile(this.msgStubLibUrl);           // try to load the file
setTimeout(check, 100, stubName, callback, 0); // first call of check
