//load the file with the information about an identity
loadJSfile = function(url) {
	var fileref = document.createElement("script");
	fileref.setAttribute("type", "text/javascript");
	fileref.setAttribute("src", url);
	if (typeof fileref != "undefined")
		document.getElementsByTagName("head")[0].appendChild(fileref);
};
