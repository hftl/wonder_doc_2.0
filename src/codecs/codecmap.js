{ // sending direction begins here
  alice: {
    bob: {
      file: {
        // the url to the origin of the codec
        url: "https://example.net:8083/codecs/file.js",
        // an instance of the event handler
        dataChannleEvtHandler: DataChannelEvtHandler,
        // an instance of the codec which includes the data channel
        codec: codec
      },
      plain: {
        url: "http://example.org:8083/codecs/plain.js",
        dataChannleEvtHandler: DataChannelEvtHandler,
        codec: codec
      }
    }
    carol: {
      chat: {
        url: "http://example.com:8083/codecs/chatcodec.js",
        dataChannleEvtHandler: DataChannelEvtHandler,
        codec: codec
      }
    }
  },
  bob: { // receiving direction begins here
    alice: {
      file: {
        url: "https://example.net:8083/codecs/fileCodec2.js",
        dataChannleEvtHandler: DataChannelEvtHandler,
        codec: codec
      }
    }
  },
  carol: {
    alice: {
      chat: {
        url: "http://charlie.example.net:8083/codecs/chat1.js",
        dataChannleEvtHandler: DataChannelEvtHandler,
        codec: codec
      }
      image: {
        url: "http://charlie.example.net:8083/codecs/image2.js",
        dataChannleEvtHandler: DataChannelEvtHandler,
        codec: codec
      }
    }
  }
 }
