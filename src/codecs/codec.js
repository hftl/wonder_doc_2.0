define(function(require, exports, module) {
  class Codec {
    constructor(dataChannel, onMessage) {
      this.dataChannel = dataChannel;
      this.onMessage = onMessage;
    }
    send(input, dataChannel) {
      // ... encode data ...
      // when used as a geneal codec for many data channels
      if (dataChannel) dataChannel.send(input);
      // when instanciated only for a particular channel
      else this.dataChannel.send(input);
    }
    onDataMessage(dataMsg) {
      // ... decode data ...
      this.onMessage(dataMsg);
    }
  }
  return new Codec();
});
