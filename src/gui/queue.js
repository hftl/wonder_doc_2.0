define(["wonder" /* ... */ ], function(wonder, config) {
  var dcCreated = false; // initial value
  var msgQueue = [];     // queue for chat messages
  var msgBtn = document.getElementById("sendMessageBtn");
  var login = document.getElementById("loginInput").value;
  var rcpt = document.getElementById("rcpt").value;
  var msgIn = document.getElementById("messageText").value;
  var chatBox = document.getElementById("chatView");

  /* ... login, logout, ... */

  msgBtn.onclick = function() { // proceed if msgBtn was clicked
    var msg = { from: login, to: rcpt, content: msgIn }
    msgQueue.push(msg); // push message into queue
    if(!dcCreated) wonder.call(rcpt, {data: "chat"}); // create dc
    else sendMessages("chat"); // send via existing dc
  }

  wonder.onMessage = function(msg, conversationId){
    switch (msg.type) {
      case MessageType.invitation:
        var confirm = confirm("new call, accept?");
        confirm == true ? wonder.answerRequest(msg, true)
                        : wonder.answerRequest(msg, false);
        break;
      case MessageType.accepted: dcCreated = true;
    }
  }
  wonder.onRtcEvt = function(evt, conversationId){
    switch (evt.type) {
      case RtcEvtType.ondatachannel:
        // send all buffered messages now
        sendMessages(evt.channel.payloadType);
        break;
    }
  }
  wonder.onDataChannelEvt = function(evt, conversationId){
    switch (evt.type) {
      case DataChannelEvtType.onmessage:
        var parsedData = JSON.parse(evt.data);
        /* ... append parsedData to view ... */
        break;
    }
  }

  function sendMessages(payloadType){
    if(msgQueue.length > 0){
      wonder.dataChannelMsg(msgQueue[0], payloadType).then(function(){
        msgQueue.splice(0, 1); // delete current element
        sendMessages(payloadType); // send next message from queue
      });
    }
  }
});
