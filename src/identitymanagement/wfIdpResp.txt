HTTP/1.1 200 OK
Access-Control-Allow-Origin: *
Content-Type: application/jrd+json

{
  subject: "alice@localhost:9110",
  aliases: ["alice@localhost", "alice@mydomain"],
  properties: {
    "http://example.de/stubs/MessagingStub_NodeJS.js" : "localStub",
    "http://example.org/codecs/plain.js" : "codec_plain",
    "http://example.com/codecs/chat.js" : "codec_chat",
    "http://example.net/codecs/file.js" : "codec_file",
    "ws://example.com:1337" : "messagingServer"
  },
  links: [ /* additional linked information */ ]
}
