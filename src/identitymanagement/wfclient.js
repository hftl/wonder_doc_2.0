var webfinger = new WebFinger({
  webfist_fallback: false, // fallback to webfist
  tls_only: false,         // only tls requests
  uri_fallback: true,      // defaults to false
  request_timeout: 10000,  // defaults to 10000
});
webfinger.lookup(rtcIdentity, function(err, data) {
  console.log(data);
}
