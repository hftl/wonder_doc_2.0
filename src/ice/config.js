{
  "iceServers": [
    {   // STUN Server:
      "url": "stun:stun.example.com:13579"
    }, { // TURN Server:
      "url": "turn:turn.example.org:24680?transport=udp",
      "credential": "credential",
      "username": "user@example.org"
    }
    /* ... */
  ]
}
