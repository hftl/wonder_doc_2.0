define("MessagingStub_NodeJS", function (require, exports, module) {
  function MessagingStub_NodeJS() {
  		// ...
  		this.signaling_server = module.config().connectURL;
  }
  // ...
  MessagingStub_NodeJS.prototype.connect = function (ownRtcIdentity, credentials, callbackFunction) {
    // ...
    this.websocket.onmessage = function (full_message) {
      var message = JSON.parse(full_message.data).body;
      Idp.getInstance().createIdentity(message.from, function (identity) {
        message.from = identity;
        Idp.getInstance().createIdentities(message.to, function (identityArr) {
          message.to = identityArr;
          that.baseStub.sendOtherMessages(message);
        });
      });
    };
  }
  return new MessagingStub_NodeJS();
});
