#!/bin/bash
if ($# == 0)
then
   echo "usage: ./eps.sh wonderv1/login.mscg"
fi

unamestr=`uname`
if [[ "$unamestr" == 'Linux' ]]; then
	mscg="$1"
	svg=$(echo "$1" | sed -e 's/\.[^\.]\+$//')".svg"
	mscgen -T svg -i "$1" -o "$svg" && chromium "$svg"
else
	# probably darwin without xdg-open
	mscg="$1"
        svg=$(echo "$1" | sed -e 's/\.[^\.]\+$//')".svg"
	mscgen -T svg -i "$1" -o "$svg" && open "$svg"
fi

