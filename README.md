# WONDER 2.0 Documentation

## Folder structure
* `main.tex` - main-document
* `packages.tex` - LaTeX packages and definitions which were used
* `cover.tex` - cover sheet
* `acr.tex` - A list of acronyms used in documentation
* `sources.bib` - used sources in BibTeX-syntax
* `./` - document root
  * `./sec` - sections (included files)
  * `./pic` - graphic files
  * `./src` - code snippets for LaTeX-document
  * `./gv` - `.gv`-files (Graphviz)
  * `./msc` - message sequence charts und LaTeX files
  * `./uml` - `.mdj` (Star-Uml)-files

## Used Tools
* LaTeX distribution/ TeXStudio (`.tex`-files) [LaTeX](http://www.latex-project.org/)
  * Linux (Debian): `sudo apt-get install texstudio texlive-*`
     * console: `pdflatex main.tex`
  * Windows: [install MiKTeX](http://miktex.org/download)
  * Mac OSX: [install MacTeX](https://tug.org/mactex/)
     * console: `pdflatex main.tex`
  * [TexStudio](http://texstudio.sourceforge.net/)
    * IDE for LaTeX (write, compile, preview)
* mscgen (`.mscg`-files)
  * Linux (Debian): `sudo apt-get install mscgen`
  * [instruction, description](http://www.mcternan.me.uk/mscgen/)
  * MscTeX ([LaTeX integration](https://github.com/unjello/msctexen))
* Graphviz (`.gv`-files)
  * Linux (Debian): `sudo apt-get install graphviz`
  * uses `DOT-language`
  * [install Graphviz](http://www.graphviz.org/Download.php)
* yEd (`.graphml`-files)
  * Tool for charts und schedules
  * [install yEd](http://www.yworks.com/en/downloads.html#yEd)
* StarUML (`.mdj`-files)
  * Tool for UML
  * [install StarUML](http://staruml.io/download)